import * as ActionTypes from './ActionTypes';
import { DISHES } from '../shared/dishes';

export const addComment = (dishId, rating, author, comment) => ({
  type: ActionTypes.ADD_COMMENT,
  payload: {
    dishId: dishId,
    rating: rating,
    author: author,
    comment: comment
  }
});
// return a funtion with an inner function
export const fetchDishes = () => (dispatch) => {
  dispatch(dishesLoading(true));

  setTimeout(() => {
    dispatch(addDishes(DISHES));
  },2000);
}

//this returns an action: DISHES_LOADING
export const dishesLoading = () => ({
  type:ActionTypes.DISHES_LOADING
});

export const dishesFailed = (err) => ({
  type:ActionTypes.DISHES_FAILED,
  payload: err
});

export const addDishes = (dishes) => ({
  type:ActionTypes.ADD_DISHES,
  payload: dishes
})
