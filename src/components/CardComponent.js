/*
Task 2

In this task you will be adding a card component to the DishdetailComponent view
to display the details of the dish given above:

Implement a function named renderDish() that takes the dish as a parameter
and returns the JSX code for laying out the details of the dish in
a reactstrap Card. You have already seen this as part of the MenuComponent
class in the exercise earlier.
Display the name of the dish as the Card title,
and the description as the Card text.
*/

import React, { Component } from 'react';
import { Card, CardImg, CardImgOverlay, CardText, CardBody, CardTitle } from 'reactstrap';

class DishCard extends Component{
  constructor(props){
    super(props);
  }

  render(){
    let dish = this.props.dish;
    return(
      <Card>
        <CardImg width="100%" src={dish.image} alt={dish.name}></CardImg>
        <CardBody>
          <CardTitle>{dish.name}</CardTitle>
          <CardText>{dish.description}</CardText>
        </CardBody>
      </Card>
    );
  }
}

export default DishCard;
