import React, { Component } from 'react';
import { Card, CardImg, CardImgOverlay, CardTitle, Media} from 'reactstrap';
import { Loading } from './LoadingComponent';

class Menu extends Component{

  render(){
    const menu = this.props.dishes.dishes.map(dish => {
      return (
        <div key={dish.id} className="col-12 col-md-5 m-1">
          <Card onClick={() => this.props.onClick(dish.id)}>
            <CardImg width="100%" src={dish.image} alt={dish.name}></CardImg>
            <CardImgOverlay>
              <CardTitle>{dish.name}</CardTitle>
            </CardImgOverlay>
          </Card>
        </div>
      );
    });
    console.log('Menu component props', this.props);

    if(this.props.dishes.isLoading){
      return (
        <div className="container">
          <div className="row">
            <Loading />
          </div>
        </div>
      );
    }else if(this.props.dishes.err){
      return (
        <div className="container">
          <div className="row">
            <h4>{this.props.dishes.err}</h4>
          </div>
        </div>
      );
    }else{
      return(
        <div className="container">
          <div className="row">
              {menu}
          </div>
        </div>
      );
    }
  }

  componentDidMount(){
    console.log('A MenuComponent is mounted');
  }
}

export default Menu;
