import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem} from 'reactstrap';

class Comments extends Component {
  render(){
    const comments = this.props.comments;

    if(comments != null){
      console.log('render comments', comments);
      return(
        comments.map(c => {
          return(
            <li key={c.id} className="list-unstyled">
              <p>{c.comment}</p>
              <p>--{c.author},{new Intl.DateTimeFormat('en-US', {year:'numeric', month:'short', day:'2-digit'}).format(new Date(Date.parse(c.date)))}</p>
            </li>
          )
        })
      );
    }else{
      return(
        <div></div>
      );
    }
  }
}

export default Comments;
