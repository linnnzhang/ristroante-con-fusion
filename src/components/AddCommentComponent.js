import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem,  Modal, ModalHeader, ModalBody, Button, Form, FormGroup, Label, Input, Col, FormFeedback, Row,
  Navbar, Nav, NavbarToggler, NavItem, } from 'reactstrap';
import { Control, LocalForm, Errors } from 'react-redux-form';

const required = (val) => val && val.length;
const maxLength = (length) => (val) => !(val) || (val.length <=length);
const minLength = (length) => (val) => !(val) || (val.length >= length);

class AddComment extends Component{
  constructor(props){
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.toggleModal = this.toggleModal.bind(this);

    this.state = {
      isModalOpen: false,
      dishId: this.props.dishId,
      addComment: this.props.addComment
    }

  }

  handleSubmit(values){
    console.log("current state: "+ JSON.stringify(values));
    this.toggleModal();
    this.props.addComment(this.props.dishId, values.rating, values.fullname, values.comment);
  }

  toggleModal(){
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  }

  render(){
    return(
      <React.Fragment>
      <Nav className="ml-auto" navbar>
        <NavItem>
          <Button onClick={this.toggleModal}>
            <span className="fa fa-sign-in fa-lg"></span>Submit a review
          </Button>
        </NavItem>
      </Nav>

      <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
        <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
        <ModalBody>
          <LocalForm onSubmit={(values) => this.handleSubmit(values)}>

            <Row className="form-group">
              <Label htmlFor="username" md={10}> Rating</Label>
              <Col md={10}>
                <Control.select model=".rating" type="select"
                name="rating"
                className="form-control"
                >
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
               </Control.select>
              </Col>
            </Row>

            <Row className="form-group">
                <Label htmlFor="username" md={10}>Your Name</Label>
                <Col md={10}>
                    <Control.text model=".fullname" type="text" id="fullname" name="fullname"
                    placeholder="full name"
                    className="form-control"
                    validators={{
                        required, minLength: minLength(3), maxLength: maxLength(15)
                    }}
                    />
                    <Errors
                        className="text-danger"
                        model=".fullname"
                        show="touched"
                        messages={{
                            required: 'Required',
                            minLength: 'Must be greater than 2 characters',
                            maxLength: 'Must be 15 characters or less'
                        }}
                     />
                </Col>
            </Row>

            <Row className="form-group">
                <Label htmlFor="comment" md={10}>Comment</Label>
                <Col md={10}>
                  <Control.textarea model=".comment" type="textarea" id="comment" name="comment"
                  placeholder="comment"
                  className="form-control"
                  validators={{
                      required, minLength: minLength(3), maxLength: maxLength(500)
                  }}
                  />
                  <Errors
                      className="text-danger"
                      model=".comment"
                      show="touched"
                      messages={{
                          required: 'Required',
                          minLength: 'Must be greater than 2 characters',
                          maxLength: 'Must be 500 characters or less'
                      }}
                   />
                 </Col>
              </Row>
              <Row className="form-group">
                <Col md={{size:10}}>
                  <Button type="submit" value="submit" color="primary">
                  Submit review</Button>
                </Col>
              </Row>
          </LocalForm>
        </ModalBody>
      </Modal>
      </React.Fragment>
    );
  }
}

export default AddComment;
