/*
DishdetailComponent
display the details of a selected dish.
You will then design the view for the component using the card component.
* Created a new DishdetailComponent and added it to your React application.
* Updated the view of the DishdetailComponent to display the details of the selected dish using an reactstrap card component.
* Updated the view of the DishdetailComponent to display the list of comments about the dish using the Bootstrap unstyled list component.
*/

import React, { Component } from 'react';
import DishCard from './CardComponent.js';
import Comments from './RenderCommentsComponent.js';
import AddComment from './AddCommentComponent.js'
import { Breadcrumb, BreadcrumbItem} from 'reactstrap';
import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent.js'
// function RenderComments({comments}){
//   if(comments != null){
//     console.log('render comments', comments);
//     return(
//       comments.map(c => {
//         return(
//           <li key={c.id} className="list-unstyled">
//             <p>{c.comment}</p>
//             <p>--{c.author},{new Intl.DateTimeFormat('en-US', {year:'numeric', month:'short', day:'2-digit'}).format(new Date(Date.parse(c.date)))}</p>
//           </li>
//         )
//       })
//     );
//   }else{
//     return(
//       <div></div>
//     );
//   }
// }

class DishDetail extends Component{
  render(){
    console.log('DishDetail component did render');

    let dish = this.props.dish;
    for(let p in dish){
      console.log(p, dish[p]);
    }
    if(this.props.isLoading){
      return (
        <div className="container">
          <div className="row">
            <Loading />
          </div>
        </div>
      );
    }else if(this.props.err){
      return (
        <div className="container">
          <div className="row">
            <h4>{this.props.err}</h4>
          </div>
        </div>
      );
    }

    if(dish != null){
      return(
      <div className="container">
        <div className="row">
          <Breadcrumb>
            <BreadcrumbItem>
              <Link to='/Menu'>Menu</Link>
            </BreadcrumbItem>

            <BreadcrumbItem active>
              {dish.name}
            </BreadcrumbItem>
          </Breadcrumb>
          <div className="col-12">
            <h3>{dish.name}</h3>
            <hr />
          </div>
        </div>
        <div className="row row-content">
          <div className="col-12 col-md-5 m-1">
             <DishCard dish = {dish}/>
          </div>
          <div className="col-12 col-md-5 m-1">
            <h4>Comments </h4>
            <Comments comments = {this.props.comments} />
            <AddComment addComment={this.props.addComment}
            dishId={this.props.dish.id}/>
          </div>
        </div>
      </div>
      );
      }else{
        return(
          <AddComment />
        );
      }
  }
}

export default DishDetail;
// <DishCard dish = { dish }/>
