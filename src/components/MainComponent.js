import React, { Component } from 'react';
import { Navbar, NavbarBrand} from 'reactstrap';
// import Menu from './MenuComponent.js'
import Menu from './MenuFunctionalComponent.js';
import DishDetail from './DishdetailComponent.js';
// import DishDetail from './DishdetailFunctionalComponent.js';
import Header from './HeaderComponent.js';
import Footer from './FooterComponent.js';
import Home from './HomeComponent.js';
import Contact from './ContactReduxComponent.js';
import About from './AboutComponent.js';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { addComment, fetchDishes } from '../redux/ActionCreators';

const mapStateToProps = state =>{
  return{
    dishes: state.dishes,
    comments: state.comments,
    promotions: state.promotions,
    leaders: state.leaders
  }
}

const mapDispatchToProps = (dispatch) => ({
  addComment: (dishId, rating, author, comment) => dispatch(addComment(dishId, rating, author, comment)),
  fetchDishes: () => {dispatch(fetchDishes())}
});

class Main extends Component {
  //lifted up to App.js from MenuComponent
  constructor(props){
    super(props);
  }

  // onDishSelect(dishId){
  //   this.setState({
  //     selectedDish: dishId
  //   });
  //   console.log('onDishSelect', this.selectedDish, 'dishid', dishId);
  // }
  componentDidMount(){
    this.props.fetchDishes();
  }
  render() {
    const HomePage = () => {
      return(
        <div className="row row-content">
          <Home dish={this.props.dishes.dishes.filter((dish) => dish.featured)[0]}
          dishesLoading={this.props.dishes.isLoading}
          dishesErr={this.props.dishes.err}
          promotions={this.props.promotions.filter((prom) => prom.featured)[0]}
          leaders = {this.props.leaders.filter((leader) => leader.featured)[0]}/>
        </div>
      );
    }

    const DishWithId = ({match}) => {
      return(
        <DishDetail dish={this.props.dishes.dishes.filter((dish) => dish.id === parseInt(match.params.dishId, 10))[0]}
          isLoading={this.props.dishes.isLoading}
          err={this.props.dishes.err}
          comments={this.props.comments.filter((comment) => comment.dishId === parseInt(match.params.dishId, 10))}
          addComment={this.props.addComment}
        />
      );
    }

    return (
      <div>
        <Header />
          <Switch>
            <Route path="/home" component={HomePage} />
            <Route exact path="/menu" component={() => <Menu dishes={this.props.dishes}/>} />
            <Route path="/menu/:dishId" component={DishWithId} />
            <Route exact path="/contactus" component={Contact} />
            <Route exact path="/aboutus" component={() => <About leaders={this.props.leaders}/>} />
            <Redirect to="/home" />
          </Switch>
        <Footer />
      </div>
    );
  }

}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));
