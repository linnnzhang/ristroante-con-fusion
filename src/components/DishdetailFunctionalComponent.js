/*
DishdetailComponent
display the details of a selected dish.
You will then design the view for the component using the card component.
* Created a new DishdetailComponent and added it to your React application.
* Updated the view of the DishdetailComponent to display the details of the selected dish using an reactstrap card component.
* Updated the view of the DishdetailComponent to display the list of comments about the dish using the Bootstrap unstyled list component.
*/

/*
Task 1
In this task you will be adding a new DishdetailComponent to your React application
and include the component into the menu component's view so that the details of a specific dish are displayed there:
* Replace the card showing the selected dish in MenuComponent's view with the DishdetailComponent,
 and make sure to pass the selected dish information as props to the DishdetailComponent.
* Create a new DishDetail class in a file named DishdetailComponent.js in the components folder
* Export the DishDetail class from this file so that it can be imported in MenuComponent.js
and used to construct the view of the selected dish.
* Return a <div> from the render() function.
This <div> should use the Bootstrap row class to position the content within the <div>.
This div will display both the details of the dish in a Card and the list of comments side-by-side for medium to extra large screens,
but will stack them for xs and sm screens.
* The card should be enclosed inside a <div> appropriate Bootstrap column classes
so that it occupies the entire 12 columns for the xs and sm screen sizes,
and 5 columns for md screens and above. Also apply a class of m-1 to this div.
* The comments should be enclosed in a <div> to which you apply appropriate column classes
so that it occupies the entire 12 columns for the xs and sm screen sizes, and 5 columns for md screens and above.
Also apply a class of m-1 to this div.
* If the dish is null then you should return an empty <div>*/

import React from 'react';
import DishCard from './CardComponent.js';
import { Breadcrumb, BreadcrumbItem} from 'reactstrap';
import { Link } from 'react-router-dom';

function RenderComments({comments}){
  if(comments != null){
    console.log('render comments', comments, 'type', comments instanceof Array);

    return(
      comments.map(c => {
        return(
          <li key={c.id} className="list-unstyled">
            <p>{c.comment}</p>
            <p>--{c.author},{new Intl.DateTimeFormat('en-US', {year:'numeric', month:'short', day:'2-digit'}).format(new Date(Date.parse(c.date)))}</p>
          </li>
        )
      })

    );
  }else{
    return(
      <div></div>
    );
  }
}

const DishDetail = (props) => {
  console.log('DishDetail component did render');

  let dish = props.dish;
  if(dish != null){
      return(
      <div className="container">
        <div className="row">
          <Breadcrumb>
            <BreadcrumbItem>
              <Link to='/Menu'>Menu</Link>
            </BreadcrumbItem>

            <BreadcrumbItem active>
              {props.dish.name}
            </BreadcrumbItem>
          </Breadcrumb>
          <div className="col-12">
            <h3>{props.dish.name}</h3>
            <hr />
          </div>
        </div>
        <div className="row row-content">
          <div className="col-12 col-md-5 m-1">
             <DishCard dish = {dish}/>
          </div>
          <div className="col-12 col-md-5 m-1">
            <h4>Comments </h4>
            <RenderComments comments = {props.comments} />
          </div>
        </div>
      </div>
      );
    }else{
      return(
        <div></div>
      );
    }
}

export default DishDetail;
// <DishCard dish = { dish }/>
